How To openSUSE
====

This repo has notes/docs and scripts to help do things on openSUSE. Sometimes I put things here to help me remember what I did. Sometimes I put things here in the hope that someone else might find it helpful to them.

`install_d.bash`
---
This script will install the language:devel:D project repo from the Open Build Service and then install dmd.

`install_nim.bash`
---
This script will install the language:devel:misc project repo from the Open Build Service and then install nim.

